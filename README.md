# CS7NS4 Daemon

This daemon handles two things:

- Fetching and storing hourly weather data from https://met.ie/api (using package `gitlab.com/johnstcn/gometie`)
- Storing cell data measurements

Data is persisted to a local SQLite database.

Usage:
```bash
  -apikeys string
    	path to text file containing api keys (default "keys.txt")
  -dbpath string
    	path to sqlite db (default "./cs7ns4.db")
  -fetch_interval int
    	interval in seconds between weather fetches (default 3600)
  -hostport string
    	host:port (default "127.0.0.1:8000")

```

## API Keys
Keys must be one per line in a file specified by `-apikeys`.

## DB Path
DB will be created at path `-dbpath` and initialized if not already present. 

## Fetch Interval
By default, we will fetch hourly reports every hour. If you're impatient, set `-fetch_interval` to something lower.

## Example cURLs
```bash
# Query Weather Data
curl -H 'X-Api-Key: weakpass' 'http://localhost:8000/weather?since=0&count=1'
[{"ts":1572620400,"name":"Valentia","temp":13,"rainfall":0,"humidity":97,"pressure":985}]

# Query Cell Info
curl -H 'X-Api-Key: weakpass' 'http://localhost:8000/cell?since=0&count=1'
[{"ts":0,"type":"test","cid":12,"taclac":23,"mcc":34,"dbm":45,"asu":56,"lvl":67,"lat":78,"lng":89,"alt":90,"acc":0}]

# Store Cell Info Measurement
curl -H 'X-Api-Key: weakpass' 'http://localhost:8000/cell' -XPOST --data '[{"ts":0,"type":"test","cid":12,"taclac":23,"mcc":34,"dbm":45,"asu":56,"lvl":67,"lat":78,"lng":89,"alt":90,"acc":0}]'

```