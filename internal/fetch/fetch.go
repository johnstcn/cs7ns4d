package fetch

import (
	"log"
	"time"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/store"
	"gitlab.com/johnstcn/gometie/pkg/met"
)

func New(dao *store.Store, client met.Client, interval time.Duration) *WeatherFetcher {
	// Start background fetcher
	stopCh := make(chan bool)
	return &WeatherFetcher{
		dao:       dao,
		metClient: client,
		ticker:    time.Tick(interval),
		stopCh:    stopCh,
	}
}

type WeatherFetcher struct {
	dao       *store.Store
	metClient met.Client
	ticker    <-chan time.Time
	stopCh    chan bool
}

func (f *WeatherFetcher) Start() {
	go func() {
		// do it once
		f.fetch()
		// then do it until we Stop
		for {
			select {
			case <-f.ticker:
				f.fetch()
			case <-f.stopCh:
				log.Println("stopping weather fetcher")
				return
			}
		}
	}()
}

func (f *WeatherFetcher) Stop() {
	f.stopCh <- true
}

func (f *WeatherFetcher) fetchStations() error {
	stations, err := f.metClient.Stations()
	if err != nil {
		log.Printf("fetching stations: %s\n", err)
		return err
	}

	ws := make([]store.WeatherStation, 0, len(stations))
	for _, station := range stations {
		s := store.WeatherStation{
			Name: station.Name,
			Slug: station.Slug,
			Lat:  station.Lat,
			Lng:  station.Lng,
		}
		ws = append(ws, s)
	}

	if err := f.dao.InsertWeatherStations(ws); err != nil {
		return err
	}

	return nil
}

func (f *WeatherFetcher) fetch() {
	if err := f.fetchStations(); err != nil {
		log.Printf("fetching stations from met.ie: %s\n" ,err)
		return
	}

	stations, err := f.dao.GetWeatherStations()
	if err != nil {
		log.Printf("fetching stored stations from db: %s\n", err)
		return
	}

	weatherInfo := make([]store.WeatherInfo, 0)
	for _, s := range stations {
		log.Println("fetching weather data for", s)
		stationReports, err := f.metClient.HourlyReports(s.Slug)
		if err != nil {
			log.Printf("fetching reports for station %s\n", err)
		}
		for _, r := range stationReports {
			weatherInfo = append(weatherInfo, store.WeatherInfoFromHourlyReport(r, s))
		}
	}

	if len(weatherInfo) == 0 {
		log.Println("no weather data fetched this turn")
		return
	}

	if err := f.dao.InsertWeatherInfos(weatherInfo); err != nil {
		log.Printf("persisting %d weather reports: %s\n", len(weatherInfo), err)
	}

	log.Printf("fetched %d weather reports\n", len(weatherInfo))
}
