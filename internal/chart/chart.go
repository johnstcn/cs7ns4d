package chart

import (
	"fmt"
	"log"
	"sort"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/geo"
	"gitlab.com/johnstcn/cs7ns4_daemon/internal/store"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
)

func New(store *store.Store) *Charter {
	return &Charter{store: store}
}

type Charter struct {
	store *store.Store
}

type stationWithDistance struct {
	store.WeatherStation
	Distance float64
}

func (c *Charter) nearbyStations(p geo.LatLng, numStations int) ([]stationWithDistance, error) {
	stations, err := c.store.GetWeatherStations()
	if err != nil {
		return nil, errors.Wrap(err, "fetching weather stations from db")
	}

	stationLocMap := make(map[geo.LatLng]store.WeatherStation)
	stationLatLngs := make([]geo.LatLng, 0, len(stations))
	for _, station := range stations {
		latLng := geo.LatLng{station.Lat, station.Lng}
		stationLocMap[latLng] = station
		stationLatLngs = append(stationLatLngs, latLng)
	}

	nearbyStationLatLngs, err := geo.KNearestHaversine(numStations, p, stationLatLngs...)
	if err != nil {
		return nil, errors.Wrap(err, "finding k-nearest weather stations")
	}
	nearbyStations := make([]stationWithDistance, 0, numStations)
	for _, sll := range nearbyStationLatLngs {
		ns := stationWithDistance{stationLocMap[sll.P], sll.Dist}
		nearbyStations = append(nearbyStations, ns)
	}

	return nearbyStations, nil
}

func (c *Charter) fetchWeatherForStations(nearbyStations []stationWithDistance, fromTs, toTs int) ([]store.WeatherInfo, error) {
	weatherInfo := make([]store.WeatherInfo, 0)
	for _, s := range nearbyStations {
		stationWeather, err := c.store.GetWeatherInfoForStation(s.ID, fromTs, toTs)
		if err != nil {
			log.Printf("fetch weather info for station %s: %s\n", s.Slug, err.Error())
			continue
		}
		weatherInfo = append(weatherInfo, stationWeather...)
	}
	return weatherInfo, nil
}

func groupRainfallByHour(weatherInfo []store.WeatherInfo) (timestamps []int64, rainfall [][]float64) {
	timestamps = make([]int64, 0)
	rainfall = make([][]float64, 0)
	tmp := make(map[int64][]float64)
	for _, wi := range weatherInfo {
		if _, exists := tmp[wi.Timestamp]; !exists {
			tmp[wi.Timestamp] = make([]float64, 0)
		}
		tmp[wi.Timestamp] = append(tmp[wi.Timestamp], wi.Rainfall)
	}
	keys := make([]int, 0)
	for k, _ := range tmp {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)
	for _, k := range keys {
		timestamps = append(timestamps, int64(k))
		rainfall = append(rainfall, tmp[int64(k)])
	}
	return timestamps, rainfall
}

func hourlyAverages(distances []float64, weatherInfo []store.WeatherInfo) ([]int64, []float64) {
	timestamps, rainfall := groupRainfallByHour(weatherInfo)
	hourlyAverages := make([]float64, 0)
	for i := 0; i < len(timestamps); i++ {
		avg := IDW(rainfall[i], distances, 3)
		hourlyAverages = append(hourlyAverages, avg)
	}

	return timestamps, hourlyAverages
}

func (c *Charter) PlotRainfall(p geo.LatLng, numStations int, fromTs, toTs int) (*plot.Plot, error) {
	nearbyStations, err := c.nearbyStations(p, numStations)
	if err != nil {
		return nil, err
	}

	distances := make([]float64, 0)
	for _, s := range nearbyStations {
		distances = append(distances, s.Distance)
	}

	nearbyStationWeather, err := c.fetchWeatherForStations(nearbyStations, fromTs, toTs)
	if err != nil {
		return nil, err
	}

	timestamps, hourlyAverages := hourlyAverages(distances, nearbyStationWeather)

	plt, err := plot.New()
	if err != nil {
		return nil, err
	}

	plt.Title.Text = fmt.Sprintf("Graph of Rainfall for (%0.4f,%0.4f) from %s to %s",
		p.Lat,
		p.Lng,
		time.Unix(int64(fromTs/1000), 0),
		time.Unix(int64(toTs/1000), 0),
	)
	plt.X.Label.Text = "Time"
	plt.Y.Label.Text = "Rainfall (mm)"

	xticks := plot.TimeTicks{Format: "2006-01-02\n15:04"}
	plt.X.Tick.Marker = xticks
	xys := make(plotter.XYs, 0)

	for i := 0; i < len(timestamps); i++ {
		point := plotter.XY{
			X: float64(timestamps[i] / 1000),
			Y: hourlyAverages[i],
		}
		xys = append(xys, point)
	}
	plt.Add(plotter.NewGrid())
	line, points, err := plotter.NewLinePoints(xys)
	if err != nil {
		return nil, err
	}
	plt.Add(line, points)

	return plt, nil
}

func (c *Charter) PlotCellStrength(p geo.LatLng, radiusKm float64, fromTs, toTs int) (*plot.Plot, error) {
	nearbyCellInfo, err := c.store.GetCellInfoWithin(fromTs, toTs, p.Lat, p.Lng, radiusKm)
	if err != nil {
		return nil, err
	}

	plt, err := plot.New()
	if err != nil {
		return nil, err
	}

	plt.Title.Text = fmt.Sprintf("Graph of Cell Strength within %0.2fkm of (%0.4f,%0.4f) from %s to %s",
		radiusKm,
		p.Lat,
		p.Lng,
		time.Unix(int64(fromTs/1000), 0),
		time.Unix(int64(toTs/1000), 0),
	)
	plt.X.Label.Text = "Time"
	plt.Y.Label.Text = "Cell Strength (dBM)"

	xticks := plot.TimeTicks{Format: "2006-01-02\n15:04"}
	plt.X.Tick.Marker = xticks

	xys := make(plotter.XYs, 0)

	for _, ci := range nearbyCellInfo {
		point := plotter.XY{
			X: float64(ci.Timestamp / 1000),
			Y: float64(ci.Dbm),
		}
		xys = append(xys, point)
	}
	plt.Add(plotter.NewGrid())
	line, points, err := plotter.NewLinePoints(xys)
	if err != nil {
		return nil, err
	}
	plt.Add(line, points)

	return plt, nil
}
