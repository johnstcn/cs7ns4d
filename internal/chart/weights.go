package chart

import "math"

// IDW calculates Shepard's' algorithm for a set of values and distances.
// Source: https://en.wikipedia.org/wiki/Inverse_distance_weighting
func IDW(vals, distances []float64, p float64) float64 {
	n := len(vals)
	var sumWeightedSamples, sumWeights float64
	for i := 0; i < n; i++ {
		if distances[i] == 0 {
			return vals[i]
		}

		weight := 1 / math.Pow(distances[i], p)
		sumWeightedSamples += weight * vals[i]
		sumWeights += weight
	}
	return sumWeightedSamples / sumWeights
}
