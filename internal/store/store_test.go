package store

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Values(t *testing.T) {
	assert.Equal(t,"(?,?),(?,?)", values(2, 2))
	assert.Equal(t,"(?,?,?),(?,?,?),(?,?,?),(?,?,?)", values(3, 4))
}
