package store

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/geo"

	"github.com/pkg/errors"
	"gitlab.com/johnstcn/gometie/pkg/met"
)

const (
	SqlCreateTables = `
CREATE TABLE IF NOT EXISTS cell_info (
	id INTEGER PRIMARY KEY,
	ts INTEGER,
	type TEXT,
	cid INTEGER,
	taclac INTEGER,
	mcc INTEGER,
	mnc INTEGER,
	dbm INTEGER,
	asu INTEGER,
	lvl INTEGER,
	lat REAL,
	lng REAL,
	alt REAL,
	acc REAL
);
CREATE TABLE IF NOT EXISTS weather_info (
	ts INTEGER,
	station_id INTEGER,
	temp INTEGER,
	rainfall REAL,
	humidity INTEGER,
	pressure INTEGER,
	PRIMARY KEY (ts, station_id)
);
CREATE TABLE IF NOT EXISTS weather_stations (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	slug TEXT NOT NULL,
	lat REAL NOT NULL,
	lng REAL NOT NULL,
	UNIQUE(slug, lat, lng)
);
`
	sqlInsertStations           = `INSERT OR IGNORE INTO weather_stations (name, slug, lat, lng) VALUES %s;`
	sqlInsertCellInfoTmpl       = `INSERT INTO cell_info (ts, type, cid, taclac, mcc, mnc, dbm, asu, lvl, lat, lng, alt, acc) VALUES %s;`
	sqlInsertWeatherInfoTmpl    = `INSERT OR REPLACE INTO weather_info (ts, station_id, temp, rainfall, humidity, pressure) VALUES %s;`
	sqlGetStations              = `SELECT id, name, slug, lat, lng FROM weather_stations;`
	sqlGetStationBySlug         = `SELECT id, name, slug, lat, lng FROM weather_stations WHERE slug = ? LIMIT 1;`
	sqlGetCellInfo              = `SELECT ts, type, cid, taclac, mcc, mnc, dbm, asu, lvl, lat, lng, alt, acc FROM cell_info WHERE ts >= ? ORDER BY ts DESC LIMIT ?;`
	sqlGetCellInfoWithin        = `SELECT ts, type, cid, taclac, mcc, mnc, dbm, asu, lvl, lat, lng, alt, acc FROM cell_info WHERE ts >= ? AND ts <= ? AND lat >= ? AND lat <= ? AND lng >= ? AND lng <= ? ORDER BY ts DESC;`
	sqlGetWeatherInfo           = `SELECT ts, name, slug, lat, lng, temp, rainfall, humidity, pressure FROM weather_info JOIN weather_stations ON weather_info.station_id = weather_stations.id WHERE ts >= ? AND ts <= ? ORDER BY ts DESC;`
	sqlGetWeatherInfoForStation = `SELECT ts, name, slug, lat, lng, temp, rainfall, humidity, pressure FROM weather_info JOIN weather_stations ON weather_info.station_id = weather_stations.id WHERE ts >= ? AND ts <= ? AND station_id = ? ORDER BY ts DESC;`
)

type WeatherStation struct {
	ID   int64   `json:"-"`
	Name string  `json:"name"`
	Slug string  `json:"slug"`
	Lat  float64 `json:"lat"`
	Lng  float64 `json:"lng"`
}

type WeatherInfo struct {
	WeatherStation
	Timestamp int64   `json:"ts"`
	Temp      int     `json:"temp"`
	Rainfall  float64 `json:"rainfall"`
	Humidity  int     `json:"humidity"`
	Pressure  int     `json:"pressure"`
}

func WeatherInfoFromHourlyReport(r met.HourlyReport, s WeatherStation) WeatherInfo {
	return WeatherInfo{
		WeatherStation: s,
		Timestamp:      r.Date.UnixNano() / int64(time.Millisecond),
		Temp:           r.Temperature,
		Rainfall:       r.Rainfall,
		Humidity:       r.Humidity,
		Pressure:       r.Pressure,
	}
}

type CellInfo struct {
	Timestamp   int64   `json:"ts"`
	NetworkType string  `json:"type"`
	CID         int     `json:"cid"`
	TacLac      int     `json:"taclac"`
	Mcc         int     `json:"mcc"`
	Mnc         int     `json:"mnc"`
	Dbm         int     `json:"dbm"`
	Asu         int     `json:"asu"`
	Lvl         int     `json:"lvl"`
	Lat         float64 `json:"lat"`
	Lng         float64 `json:"lng"`
	Alt         float64 `json:"alt"`
	Acc         float64 `json:"acc"`
}

func values(cols, rows int) string {
	b := strings.Builder{}
	for i := 0; i < rows; i++ {
		b.WriteByte('(')
		for j := 0; j < cols; j++ {
			b.WriteByte('?')
			if j != cols-1 {
				b.WriteByte(',')
			}
		}
		b.WriteByte(')')
		if i != rows-1 {
			b.WriteByte(',')
		}
	}
	return b.String()
}

// store handles database interactions
type Store struct {
	db      *sql.DB
	verbose bool
}

func New(db *sql.DB, verbose bool) *Store {
	return &Store{
		db:      db,
		verbose: verbose,
	}
}

func (s *Store) GetWeatherStations() ([]WeatherStation, error) {
	if s.verbose {
		log.Printf("db: query: %s\n", sqlGetStations)
	}
	rows, err := s.db.Query(sqlGetStations)
	if err != nil {
		return nil, errors.Wrap(err, "query rows from db")
	}

	stations := make([]WeatherStation, 0)
	for rows.Next() {
		var s WeatherStation
		if err := rows.Scan(&s.ID, &s.Name, &s.Slug, &s.Lat, &s.Lng); err != nil {
			log.Printf("scanning station from row: %s\n", err)
		}
		stations = append(stations, s)
	}

	return stations, nil
}

func (s *Store) GetWeatherStationBySlug(slug string) (WeatherStation, error) {
	if s.verbose {
		log.Printf("db: query: %s %s\n", sqlGetStationBySlug, slug)
	}

	rows, err := s.db.Query(sqlGetStationBySlug, slug)
	if err != nil {
		return WeatherStation{}, errors.Wrap(err, "query rows from db")
	}

	var ws WeatherStation
	for rows.Next() {
		if err := rows.Scan(&ws.ID, &ws.Name, &ws.Slug, &ws.Lat, &ws.Lng); err != nil {
			log.Printf("scanning station from row: %s\n", err)
		}
		break
	}

	return ws, nil
}

func (s *Store) GetCellInfo(sinceTs, limit int) ([]CellInfo, error) {
	if s.verbose {
		log.Printf("db: query: %s %d %d\n", sqlGetCellInfo, sinceTs, limit)
	}
	rows, err := s.db.Query(sqlGetCellInfo, sinceTs, limit)
	if err != nil {
		return nil, errors.Wrap(err, "query rows from db")
	}

	ci := make([]CellInfo, 0)
	for rows.Next() {
		var c CellInfo
		if err := rows.Scan(&c.Timestamp, &c.NetworkType, &c.CID, &c.TacLac, &c.Mcc, &c.Mnc, &c.Dbm, &c.Asu, &c.Lvl, &c.Lat, &c.Lng, &c.Alt, &c.Acc); err != nil {
			log.Printf("scanning cellinfo from row: %s\n", err)
		}
		ci = append(ci, c)
	}

	return ci, nil
}

func (s *Store) GetCellInfoWithin(fromTs, toTs int, lat, lng, radius float64) ([]CellInfo, error) {
	pMin, pMax := geo.BoundingBox(geo.LatLng{lat, lng}, radius)
	if s.verbose {
		log.Printf(
			"db: query: %s %d %d %0.4f %0.4f %0.4f %0.4f\n",
			sqlGetCellInfoWithin, fromTs, toTs, pMin.Lat, pMax.Lat, pMin.Lng, pMax.Lng,
		)
	}
	rows, err := s.db.Query(sqlGetCellInfoWithin, fromTs, toTs, pMin.Lat, pMax.Lat, pMin.Lng, pMax.Lng)
	if err != nil {
		return nil, errors.Wrap(err, "query rows from db")
	}

	ci := make([]CellInfo, 0)
	for rows.Next() {
		var c CellInfo
		if err := rows.Scan(&c.Timestamp, &c.NetworkType, &c.CID, &c.TacLac, &c.Mcc, &c.Mnc, &c.Dbm, &c.Asu, &c.Lvl, &c.Lat, &c.Lng, &c.Alt, &c.Acc); err != nil {
			log.Printf("scanning cellinfo from row: %s\n", err)
		}
		ci = append(ci, c)
	}
	return ci, nil
}

func (s *Store) InsertWeatherStations(ws []WeatherStation) error {
	tx, err := s.db.Begin()
	if err != nil {
		return errors.Wrap(err, "init db tx")
	}

	for _, station := range ws {
		query := fmt.Sprintf(sqlInsertStations, values(4, 1))
		stmt, err := tx.Prepare(query)
		if err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "prepare stmt to insert weather stations")
		}
		args := []interface{}{station.Name, station.Slug, station.Lat, station.Lng}
		if s.verbose {
			log.Printf("db: query: %station %v\n", query, args)
		}
		if _, err := stmt.Exec(args...); err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "exec stmt to insert weather station")
		}
	}

	return tx.Commit()
}

func (s *Store) InsertCellInfos(ci []CellInfo) error {
	tx, err := s.db.Begin()
	if err != nil {
		return errors.Wrap(err, "init db tx")
	}

	for _, c := range ci {
		query := fmt.Sprintf(sqlInsertCellInfoTmpl, values(13, 1))
		//log.Println("db exec:", query)
		stmt, err := tx.Prepare(query)
		if err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "prepare stmt to insert cell info")
		}
		args := []interface{}{c.Timestamp, c.NetworkType, c.CID, c.TacLac, c.Mcc, c.Mnc, c.Dbm, c.Asu, c.Lvl, c.Lat, c.Lng, c.Alt, c.Acc}
		if s.verbose {
			log.Printf("db: query: %s %v\n", query, args)
		}
		if _, err := stmt.Exec(args...); err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "exec stmt to insert cell info")
		}
	}

	return tx.Commit()
}

func (s *Store) GetWeatherInfo(fromTs, toTs int) ([]WeatherInfo, error) {
	if s.verbose {
		log.Printf("db: query: %s %d %d\n", sqlGetWeatherInfo, fromTs, toTs)
	}
	rows, err := s.db.Query(sqlGetWeatherInfo, fromTs, toTs)
	if err != nil {
		return nil, errors.Wrap(err, "query rows from db")
	}

	wi := make([]WeatherInfo, 0)
	for rows.Next() {
		var w WeatherInfo
		if err := rows.Scan(&w.Timestamp, &w.Name, &w.Slug, &w.Lat, &w.Lng, &w.Temp, &w.Rainfall, &w.Humidity, &w.Pressure); err != nil {
			log.Printf("scanning weather info from row: %s\n", err)
		}
		wi = append(wi, w)
	}

	return wi, nil
}

func (s *Store) GetWeatherInfoForStation(stationID int64, fromTs, toTs int) ([]WeatherInfo, error) {
	if s.verbose {
		log.Printf("db: query: %s %d %d %d\n", sqlGetWeatherInfoForStation, fromTs, toTs, stationID)
	}
	rows, err := s.db.Query(sqlGetWeatherInfoForStation, fromTs, toTs, stationID)
	if err != nil {
		return nil, errors.Wrap(err, "query rows from db")
	}

	wi := make([]WeatherInfo, 0)
	for rows.Next() {
		var w WeatherInfo
		if err := rows.Scan(&w.Timestamp, &w.Name, &w.Slug, &w.Lat, &w.Lng, &w.Temp, &w.Rainfall, &w.Humidity, &w.Pressure); err != nil {
			log.Printf("scanning weather info from row: %s\n", err)
		}
		wi = append(wi, w)
	}

	return wi, nil
}

func (s *Store) InsertWeatherInfos(wi []WeatherInfo) error {
	tx, err := s.db.Begin()
	if err != nil {
		return errors.Wrap(err, "init db tx")
	}

	for _, w := range wi {
		query := fmt.Sprintf(sqlInsertWeatherInfoTmpl, values(6, 1))
		stmt, err := tx.Prepare(query)
		if err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "prepare stmt to insert weather info")
		}

		args := []interface{}{w.Timestamp, w.WeatherStation.ID, w.Temp, w.Rainfall, w.Humidity, w.Pressure}
		if s.verbose {
			log.Printf("db: query: %s %v\n", query, args)
		}
		if _, err := stmt.Exec(args...); err != nil {
			_ = tx.Rollback()
			return errors.Wrap(err, "exec stmt to insert weather info")
		}
	}

	return tx.Commit()
}
