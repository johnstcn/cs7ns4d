package server

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/chart"
	"gitlab.com/johnstcn/cs7ns4_daemon/internal/geo"
	"gonum.org/v1/plot/vg"

	"github.com/pkg/errors"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/store"
)

const headerApiKey = "X-Api-Key"

// Server handles http 	headerApiKey = "X-Api-Key"
type Server struct {
	dao     *store.Store
	charter *chart.Charter
	apiKeys map[string]bool
}

func New(dao *store.Store, charter *chart.Charter, apiKeys map[string]bool) *Server {
	return &Server{
		dao:     dao,
		charter: charter,
		apiKeys: apiKeys,
	}
}

// various HTTP handlers for Server
func (s *Server) HandleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte(`CS7NS4 Daemon
=============

Endpoints:

	GET /cell?since=123456789&count=123
	GET /weather?from_ts=123456789&to_ts=from_ts=234567890
	GET /station_weather?station=dublin-airport&from_ts=12345689&to_ts=234567890
	GET /chart/rainfall?lat=1.23&lng=2.34&from_ts=12345689&to_ts=234567890
	GET /chart/cellstrength?lat=1.23&lng=2.34&radius=3.45&from_ts=12345689&to_ts=234567890
	POST /cell
	POST /cell?random=true

Params:
 - since: timestamp (milliseconds)
 - count: number of records

***All endpoints require a valid X-Api-Key header value.***`))
	}
}
func (s *Server) HandleCellInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			s.handleCellInfoGet(w, r)
		case http.MethodPost:
			s.handleCellInfoPost(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func getOrDefaultInt(r *http.Request, key string, defaultValue int) int {
	rawVal := r.URL.Query().Get(key)
	parsedVal, err := strconv.Atoi(rawVal)
	if err != nil {
		return defaultValue
	}
	return parsedVal
}

func getOrDefaultFloat(r *http.Request, key string, defaultValue float64) float64 {
	rawVal := r.URL.Query().Get(key)
	parsedVal, err := strconv.ParseFloat(rawVal, 64)
	if err != nil {
		return defaultValue
	}
	return parsedVal
}

func (s *Server) handleCellInfoGet(w http.ResponseWriter, r *http.Request) {
	fromTs := getOrDefaultInt(r, "from_ts", 0)
	toTs := getOrDefaultInt(r, "to_ts", math.MaxInt64) // XXX: 29K years in the future is good enough
	lat := getOrDefaultFloat(r, "lat", 53.3498)        // Dublin
	lng := getOrDefaultFloat(r, "lng", -6.2603)        // Dublin
	radius := getOrDefaultFloat(r, "radius_kms", 1.0)
	info, err := s.dao.GetCellInfoWithin(fromTs, toTs, lat, lng, radius)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	jsonStr, err := json.Marshal(info)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonStr)
}

func (s *Server) handleCellInfoPost(w http.ResponseWriter, r *http.Request) {
	nRandom := getOrDefaultInt(r, "random", 0)
	info := make([]store.CellInfo, 0)
	if nRandom > 0 {
		info = randomCellInfo(nRandom)
	} else {
		payload, _ := ioutil.ReadAll(r.Body)
		if err := json.Unmarshal(payload, &info); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			log.Println(errors.Wrapf(err, "invalid JSON %s", payload))
			return
		}
	}

	if err := s.dao.InsertCellInfos(info); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		log.Println(err.Error())
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (s *Server) HandleWeatherInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			s.handleWeatherInfoGet(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (s *Server) HandleStationWeatherInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			s.handleStationWeatherGet(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (s *Server) handleWeatherInfoGet(w http.ResponseWriter, r *http.Request) {
	now := int(time.Now().Unix()) * 1000
	fromTs := getOrDefaultInt(r, "from_ts", now-(3600*1000))
	toTs := getOrDefaultInt(r, "to_ts", now)
	info, err := s.dao.GetWeatherInfo(fromTs, toTs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	jsonStr, err := json.Marshal(info)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonStr)
}

func (s *Server) handleStationWeatherGet(w http.ResponseWriter, r *http.Request) {
	stationSlug := r.URL.Query().Get("station")
	now := int(time.Now().Unix()) * 1000
	fromTs := getOrDefaultInt(r, "from_ts", now-(3600*1000))
	toTs := getOrDefaultInt(r, "to_ts", now)
	if stationSlug == "" {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("must specify station parameter"))
		return
	}

	station, err := s.dao.GetWeatherStationBySlug(stationSlug)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("unknown station: " + stationSlug))
		return
	}

	stationWeather, err := s.dao.GetWeatherInfoForStation(station.ID, fromTs, toTs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("fetching weather for station " + stationSlug + ": " + err.Error()))
		return
	}

	jsonStr, err := json.Marshal(stationWeather)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonStr)
}

func (s *Server) HandleChartRainfall() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			s.handleChartRainfallGet(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (s *Server) handleChartRainfallGet(w http.ResponseWriter, r *http.Request) {
	lat := getOrDefaultFloat(r, "lat", 53.3498) // Dublin
	lng := getOrDefaultFloat(r, "lng", -6.2603) // Dublin
	now := int(time.Now().Unix()) * 1000
	fromTs := getOrDefaultInt(r, "from_ts", now-(3600*1000))
	toTs := getOrDefaultInt(r, "to_ts", now)
	numStations := getOrDefaultInt(r, "k", 3)

	plt, err := s.charter.PlotRainfall(geo.LatLng{lat, lng}, numStations, fromTs, toTs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	pltWidth := 10 * vg.Inch
	pltHeight := 10 * vg.Inch
	writer, err := plt.WriterTo(pltWidth, pltHeight, "png")
	w.WriteHeader(http.StatusOK)
	_, _ = writer.WriteTo(w)
}

func (s *Server) HandleChartCellStrength() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			s.handleChartCellStrengthGet(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}

func (s *Server) handleChartCellStrengthGet(w http.ResponseWriter, r *http.Request) {
	lat := getOrDefaultFloat(r, "lat", 53.3498) // Dublin
	lng := getOrDefaultFloat(r, "lng", -6.2603) // Dublin
	radius := getOrDefaultFloat(r, "radius", -6.2603) // Dublin
	now := int(time.Now().Unix()) * 1000
	fromTs := getOrDefaultInt(r, "from_ts", now-(3600*1000))
	toTs := getOrDefaultInt(r, "to_ts", now)

	plt, err := s.charter.PlotCellStrength(geo.LatLng{lat, lng}, radius, fromTs, toTs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	pltWidth := 10 * vg.Inch
	pltHeight := 10 * vg.Inch
	writer, err := plt.WriterTo(pltWidth, pltHeight, "png")
	w.WriteHeader(http.StatusOK)
	_, _ = writer.WriteTo(w)
}

// middleware to require API key for POST requests
func (s *Server) RequireApiKey(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			// check if API key has been provided and abort request if not
			hdrVal := r.Header.Get(headerApiKey)
			if _, found := s.apiKeys[hdrVal]; !found {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}

		// otherwise, handle request normally
		h(w, r)
	}
}

func (s *Server) LogRequest(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		wrapped := &wrapper{ResponseWriter: w}
		start := time.Now()
		h(wrapped, r)
		elapsed := time.Since(start)
		log.Printf("%s %s => %d %d (%s)\n", r.Method, r.URL.String(), wrapped.code, wrapped.bytes, elapsed)
	}
}

// wrapper wraps http.ResponseWriter for logging response codes.
// Adapted from: https://gist.github.com/blixt/01d6bdf8aa8ae57d5c72c1907b6db670
type wrapper struct {
	http.ResponseWriter
	written bool
	code    int
	bytes   int64
}

func (w *wrapper) Write(b []byte) (int, error) {
	var n int
	var err error
	if !w.written {
		w.WriteHeader(http.StatusOK)
	}
	n, err = w.ResponseWriter.Write(b)
	w.bytes += int64(n)
	return n, err
}

func (w *wrapper) WriteHeader(n int) {
	w.ResponseWriter.WriteHeader(n)
	if w.written {
		return
	}
	w.written = true
	w.code = n
}
