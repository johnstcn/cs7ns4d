package server

import (
	"math/rand"
	"time"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/store"
)

func randomCellInfo(n int) []store.CellInfo {
	now := time.Now().Unix()
	// * 1000 because millis
	toTs := now * 1000
	fromTs := toTs - (24 * 7 * 3600 * 1000) // one week ago
	rand.Seed(now)
	networkTypes := []string{
		"gsm",
		"cdma",
		"wcdma",
		"lte",
	}
	info := make([]store.CellInfo, 0, n)
	for i := 0; i < n; i++ {
		ci := store.CellInfo{
			Timestamp:   int64(randint(int(fromTs), int(toTs))),
			NetworkType: networkTypes[rand.Int()%len(networkTypes)],
			CID:         randint(100000, 999999),
			TacLac:      randint(1000, 9999),
			Mcc:         randint(1, 999),
			Mnc:         randint(1, 99),
			Dbm:         randint(-128, 0),
			Asu:         randint(0, 50),
			Lvl:         randint(0, 9),
			Lat:         randfloat(53.295593, 53.391519),
			Lng:         randfloat(-6.400223, -6.186676),
			Alt:         randfloat(0, 100),
			Acc:         randfloat(0, 100),
		}
		info = append(info, ci)
	}
	return info
}

func randint(min, max int) int {
	return rand.Intn(max-min) + min
}

func randfloat(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}
