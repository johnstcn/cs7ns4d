package geo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_HaversineDistance(t *testing.T) {
	p1 := LatLng{52.205, 0.119}
	p2 := LatLng{48.857, 2.351}
	expected := 404.3
	actual := DistanceKM(p1, p2)
	assert.InDelta(t, expected, actual, 0.03)
}

func Test_KNearestHaversine(t *testing.T) {
	p0 := LatLng{0, 0}
	points := []LatLng{
		{Lat: 1},
		{Lat: 2},
		{Lat: 3},
	}

	expected := []LatLngDistance{
		{
			P:    points[0],
			Dist: 111.19492664455873,
		},
		{
			P:    points[1],
			Dist: 222.38985328911747,
		},
	}
	actual, err := KNearestHaversine(2, p0, points...)
	assert.NoError(t, err)
	assert.Len(t, actual, 2)
	assert.EqualValues(t, expected[0].P, actual[0].P)
	assert.EqualValues(t, actual[1].P, actual[1].P)
	assert.InDelta(t, 111.19, actual[0].Dist, 0.01)
	assert.InDelta(t, 222.38, actual[1].Dist, 0.01)
}
