package geo

import (
	"errors"
	"math"
	"sort"
)

const EarthRadius = 6371.0

type LatLng struct {
	Lat float64
	Lng float64
}

// BoundingBox returns a bounding box for a given point (lat, lng) and a distance in km.
// Adapted from https://www.movable-type.co.uk/scripts/latlong-db.html
func BoundingBox(p LatLng, kms float64) (pMin, pMax LatLng) {
	dLat := Rad2Deg(kms / EarthRadius)
	dLng := math.Asin(kms/EarthRadius) / math.Cos(Deg2Rad(p.Lat))
	pMin = LatLng{
		Lat: p.Lat-dLat,
		Lng: p.Lng-dLng,
	}
	pMax = LatLng{
		Lat: p.Lat+dLat,
		Lng: p.Lng+dLng,
	}
	return pMin, pMax
}

// DistanceKM returns the great-circle distance between (lat1, lng1), (lat2, lng2) in km.
// Not valid for planets other than Earth.
// Adapted from http://www.movable-type.co.uk/scripts/latlong.html
//noinspection NonAsciiCharacters: forgive me for I have sinned
func DistanceKM(p1, p2 LatLng) (kms float64) {
	φ1 := Deg2Rad(p1.Lat)
	φ2 := Deg2Rad(p2.Lat)

	Δφ := Deg2Rad(p2.Lat - p1.Lat)
	Δλ := Deg2Rad(p2.Lng - p1.Lng)

	a := math.Pow(math.Sin(Δφ/2), 2) + math.Cos(φ1)*math.Cos(φ2)*math.Pow(math.Sin(Δλ/2), 2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return EarthRadius * c
}

func Rad2Deg(rad float64) float64 {
	return 180.0 * rad / math.Pi
}

func Deg2Rad(deg float64) float64 {
	return math.Pi / 180 * deg
}

// LatLngDistance is a distance of p from a given point.
type LatLngDistance struct {
	P    LatLng
	Dist float64
}

// PointDistances is a slice of LatLngDistance.
type PointDistances []LatLngDistance

// Common gopher-ism: implement sort.Interface for slice type.
// see: https://groups.google.com/d/msg/golang-nuts/FT7cjmcL7gw/Gj4_aEsE_IsJ
func (pd PointDistances) Len() int           { return len(pd) }
func (pd PointDistances) Less(i, j int) bool { return pd[i].Dist < pd[j].Dist }
func (pd PointDistances) Swap(i, j int)      { pd[i], pd[j] = pd[j], pd[i] }

// KNearestHaversine returns the k nearest points to p1 by Haversine distance.
func KNearestHaversine(k int, p1 LatLng, points ...LatLng) ([]LatLngDistance, error) {
	// TODO make it not O(2n*log(n))
	if k > len(points) {
		return nil, errors.New("k must be less than the number of points")
	}

	results := make(PointDistances, len(points))
	for i, p := range points {
		results[i] = LatLngDistance{
			P:    p,
			Dist: DistanceKM(p1, p),
		}
	}
	sort.Sort(results)
	return results[:k], nil
}
