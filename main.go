package main

import (
	"bufio"
	"database/sql"
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/chart"

	"gitlab.com/johnstcn/cs7ns4_daemon/internal/fetch"
	"gitlab.com/johnstcn/cs7ns4_daemon/internal/server"
	"gitlab.com/johnstcn/cs7ns4_daemon/internal/store"

	"gitlab.com/johnstcn/gometie/pkg/met"

	"github.com/pkg/errors"

	_ "github.com/mattn/go-sqlite3"
)

func mustInitApi(apiKeysPath string) map[string]bool {
	// open API keys file
	apiKeysFile, err := os.Open(apiKeysPath)
	if err != nil {
		log.Fatal(errors.Wrap(err, "read api keys file"))
	}
	// don't forget to close it later
	defer func() { _ = apiKeysFile.Close() }()
	// read API keys file
	scanner := bufio.NewScanner(apiKeysFile)
	apiKeys := make(map[string]bool)
	for scanner.Scan() {
		apiKeys[strings.TrimSpace(scanner.Text())] = true
	}

	return apiKeys
}

func mustInitDB(dbPath string) *sql.DB {
	// init and connect to db
	sqliteDb, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatal(errors.Wrap(err, "open sqlite db"))
	}

	if _, err := sqliteDb.Exec(store.SqlCreateTables); err != nil {
		log.Fatal(errors.Wrap(err, "exec create table statement"))
	}

	return sqliteDb
}

func initRoutes(srv *server.Server) {
	http.HandleFunc("/", srv.LogRequest(srv.HandleIndex()))
	http.HandleFunc("/cell", srv.LogRequest(srv.RequireApiKey(srv.HandleCellInfo())))
	http.HandleFunc("/weather", srv.LogRequest(srv.RequireApiKey(srv.HandleWeatherInfo())))
	http.HandleFunc("/station_weather", srv.LogRequest(srv.HandleStationWeatherInfo()))
	http.HandleFunc("/chart/rainfall", srv.LogRequest(srv.HandleChartRainfall()))
	http.HandleFunc("/chart/cellstrength", srv.LogRequest(srv.HandleChartCellStrength()))
}

func main() {
	var hostPort string
	var dbPath string
	var apiKeysPath string
	var fetchIntervalSecs int
	var fetchWeather bool
	var verbose bool
	flag.StringVar(&hostPort, "hostport", "127.0.0.1:8000", "host:port")
	flag.StringVar(&dbPath, "dbpath", "./cs7ns4.db", "path to sqlite db")
	flag.StringVar(&apiKeysPath, "apikeys", "keys.txt", "path to text file containing api keys")
	flag.IntVar(&fetchIntervalSecs, "fetch_interval", 3600, "interval in seconds between weather fetches")
	flag.BoolVar(&fetchWeather, "fetch_weather", true, "should we fetch weather info")
	flag.BoolVar(&verbose, "verbose", false, "enable/disable verbose logging")
	flag.Parse()

	apiKeys := mustInitApi(apiKeysPath)
	sqliteDb := mustInitDB(dbPath)
	dao := store.New(sqliteDb, verbose)
	charter := chart.New(dao)
	wf := fetch.New(dao, met.New(), time.Duration(fetchIntervalSecs)*time.Second)
	srv := server.New(dao, charter, apiKeys)
	initRoutes(srv)

	if fetchWeather {
		wf.Start()
		defer wf.Stop()
	}

	// finally, serve http requests
	log.Fatal(http.ListenAndServe(hostPort, nil))
}
