module gitlab.com/johnstcn/cs7ns4_daemon

go 1.12

require (
	github.com/golang/geo v0.0.0-20190916061304-5b978397cfec // indirect
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/johnstcn/gometie v0.0.7
	gonum.org/v1/plot v0.0.0-20191107103940-ca91d9d40d0a
)
